import {AppRegistry} from 'react-native';
import Coachify from './src/coachify';

//outsourcing der startinstanz (zuvor in index) für authentif. ios und android unabhängiger code
AppRegistry.registerComponent('coachify', () => Coachify);
