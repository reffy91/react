/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import CoachList from './components/coachList/coachList';
import FlatList from './components/coachList/FlatList';
import CardSection from './components/coachList/cardSection';
import Header from './components/header/header';
import Button from './components/styles/Button';
import Spinner from './components/styles/Spinner';
import firebase from 'firebase';
import LoginForm from './components/login/LoginForm'
import Footer from './components/footer/footer';



class Coachify extends Component {

  constructor(props) {
    super(props);
    this.state = { loggedIn:null };
  }

  componentWillMount(){

    //firebse configration - connect o project
    let config ={
    apiKey: "AIzaSyDJAEtU9dWCMO_A5H5tpKxF29i2XF3u4L4",
    authDomain: "coachify-d2bc5.firebaseapp.com",
    databaseURL: "https://coachify-d2bc5.firebaseio.com",
    projectId: "coachify-d2bc5",
    storageBucket: "coachify-d2bc5.appspot.com",
    messagingSenderId: "733644468369"};

    firebase.initializeApp(config);




    firebase.auth().onAuthStateChanged((user)=>{
      if(user){
        this.setState({loggedIn:true});
      }else{
        this.setState({loggedIn:false});
      }
    });
    }

    renderContent(){
      switch ((this.state.loggedIn)) {
        case true:
          return(
            <View style={{flex:1,flexDirection:'column',justifyContent:'flex-end'}}>
              <CardSection>
                <Button onPress={()=>firebase.auth().signOut()}>
                  Log out
                </Button>
              </CardSection>
            </View>
            );
        case false:  return <LoginForm/>;
        default: return <Spinner size="large"/>
    }
  }

  render() {
    return (
      //für scrollview  erweiter den container für den scroll
       /*    <CoachList/>   <CardSection>{this.renderContent()}</CardSection>  <Header headerText={'Coachify'} /><CoachList/> */
      <View style={{flex:1}}>
         <Header headerText={'Coachify'} />

         <FlatList/>
         <Footer/>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default Coachify;
