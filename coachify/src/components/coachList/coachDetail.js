import React from 'react';
import { Text,View,StyleSheet,Image,Linking} from 'react-native';
import CoachCard from './coachCard';
import CardSection from './cardSection';
import Button from '../styles/Button';

/*
if we need more than one prop attribute then it is better
to destruct the prop in attribute for need

only coach prop = ({coach }})

const {name, title coach} = coach ;  => <Text>{title}</Text>
*/
const CoachDetail = ({listi}) =>{
  //Destructor props object
  const {
    title,
    artist,
    thumbnail_image,
    image, url
  } = listi;

  const {
    imageStyle,
    headerContentStyle,
    imageContainerStyle,
    headerTextStyle,
    imagemStyle
  } = styles;

  console.log("hallo");

  return(
      /* props get the date from the parent jsx element with the attribut */
    <CoachCard>

        <CardSection>
          <View style={[imageContainerStyle]}>
            <Image style={[imageStyle]} source={{uri:thumbnail_image}}/>
          </View>

          <View style={[headerContentStyle]}>
            <Text style={[headerTextStyle]}>{title}</Text>
            <Text>{artist}</Text>
          </View>
        </CardSection>

        <CardSection>
          <Image style={[imagemStyle]} source={{uri:image}} />
        </CardSection>


        <CardSection>
          <Button onPress={()=>Linking.openURL(url)}>
          Buy Me !
          </Button>
        </CardSection>

    </CoachCard>

  );


};

const styles = StyleSheet.create({

  headerContentStyle:{
      flexDirection:'column',
      justifyContent:'space-around'
    },
    headerTextStyle:{
      fontSize:18
    },
  imageStyle:{
      height:50,
      width:50
    },

  imageContainerStyle:{
    justifyContent:'center',
    alignItems:'center',
    marginLeft:10,
    marginRight:10
  },
  imageContainerStyle:{
    justifyContent:'center',
    alignItems:'center',
    marginLeft:10,
    marginRight:10
  },
  imagemStyle:{
    height:300,
    flex:1,
    width:null

  }

});

export default CoachDetail;
