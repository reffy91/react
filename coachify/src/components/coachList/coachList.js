/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  FlatList
} from 'react-native';
import CoachDetail from './coachDetail';


export default class CoachList extends Component {
  constructor(props) {
      super(props);
      this.state = {list:[]};
};
      //LifeCircle = By Starting this Component
    componentWillMount(){
    return fetch('https://rallycoding.herokuapp.com/api/music_albums')
      .then((response)=>response.json()).then((responseData)=>this.setState({list: responseData}));
      // .setState  this is the only way to initialing data und rerender
      //. handles the http request just only for this comp bzw for this list

  };
    //ifference of props and setState
    /*props is for parent to child communication
     setstate for http/ updating communication*/

   //renderList
   renderList(){
  //map() is an array helper function
  // op1 <Detail key={listi.title} listi={listi} /> req import Details and state variabel
  //op2 return this.state.list.map(listi => <Text>{listi.title}</Text>);
  // key prop for performance
   return this.state.list.map(listi => <CoachDetail key={listi.title} listi={listi}/>);
  };


  render() {
    console.log(this.state);
    return (

    /*
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome nassir!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit index.android.js
        </Text>
        <Text style={styles.instructions}>
          Double tap R on your keyboard to reload,{'\n'}
          Shake or press menu button for dev menu
        </Text>
      </View>
    */
    /*//  <View>
        <Text>Coachify List</Text>
      </View>
    *  Nur bei static componets */
      <ScrollView>
        {this.renderList()}
      </ScrollView>

    );
  }



}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});
