

//import library
import React from 'react';
import { Text,StyleSheet,View} from 'react-native';
import { Tabs, Tab, Icon,CustomBadgeView } from 'react-native-elements';
import TabNavigator from 'react-native-tab-navigator';



//Create Component
//props: Obj parameter der daten von eltern elementen erhaelt
const Footer = (props,selectedTab,Feed,Profile)=>{

  return(

    <Tabs>
      <Tab
        titleStyle={{fontWeight: 'bold', fontSize: 10}}
        selectedTitleStyle={{marginTop: -1, marginBottom: 6}}
        selected={selectedTab === 'feed'}
        title={selectedTab === 'feed' ? 'FEED' : null}
        renderIcon={() => <Icon containerStyle={{justifyContent: 'center', alignItems: 'center', marginTop: 12}} color={'#5e6977'} name='whatshot' size={33} />}
        renderSelectedIcon={() => <Icon color={'#6296f9'} name='whatshot' size={30} />}
        onPress={() => this.changeTab('feed')}>
        <Feed />
      </Tab>
      <Tab
        titleStyle={{fontWeight: 'bold', fontSize: 10}}
        selectedTitleStyle={{marginTop: -1, marginBottom: 6}}
        selected={selectedTab === 'profile'}
        title={selectedTab === 'profile' ? 'PROFILE' : null}
        renderIcon={() => <Icon containerStyle={{justifyContent: 'center', alignItems: 'center', marginTop: 12}} color={'#5e6977'} name='person' size={33} />}
        renderSelectedIcon={() => <Icon color={'#6296f9'} name='person' size={30} />}
        onPress={() => this.changeTab('profile')}>
        <Profile />
      </Tab>
      /* ... more tabs here */
    </Tabs>

  );
};


//Stylesheet
const styles = StyleSheet.create({
  Footer:{
    height:66,
    backgroundColor: '#eee',
    shadowColor: '#000',
    shadowOffset:{width:0,height:2},
    shadowOpacity:0.9,
    elevation:2,
    flex:1,
    flexDirection:'column',
    justifyContent:'flex-end'

  },

  fonts:{
    color:'#01579b',
    fontWeight:'bold',
    fontSize:30,
    padding:10,


  }



});

//make comp avaibale to the other components in references
export default Footer;


//stylesheet : Layout,Padding, Margin, font, text-align b-/color,
// gradient,
