import React, {Component} from 'react';
import {Text,View} from 'react-native';
//todo pakete bündeln mit * ... sehe anfang von firebase kapitel
import CoachCard from '../coachList/coachCard';
import CardSection from '../coachList/cardSection';
import Button from '../styles/Button';
import Input from '../styles/input';
import Spinner from '../styles/Spinner';
import firebase from 'firebase';

class LoginForm extends Component{

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      pssw:'',
      error:'',
      loading:false
    }
  }
  onButtonPress(){
    //Give me the InputData
    const { email ,pssw} = this.state;
    console.log({email});
    //Set LogginError null , loading for SPinner true
    this.setState({error:'',loading:true});

    //FireBase Authentifikation + Account Erstellung .
    //ab then der coder der ausgeführt wird, wenn er erfolgreich angemeldet ist
    firebase.auth().signInWithEmailAndPassword(email,pssw)
    .then(this.onLoginSuccess.bind(this))
      .catch(()=>{
          firebase.auth().createUserWithEmailAndPassword(email,pssw)
          .then(this.onregisterSuccess.bind(this))
          .catch(this.onLoginFail.bind(this));
      });


  }
  //Fehlberechtigung
  onLoginFail(){
    this.setState({error:'Authentification Failed()', loading:false})
  }
  //Funktion Wenn Login erfolgreich war => navigation zu neue Componente
  onLoginSuccess(){
      this.setState({
        email:'',
        pssw:'',
        loading:false,
        error:'',

      });
    }
    //Funktion Wenn Login erfolgreich war => navigation zu neue Componente
      onregisterSuccess(){
          this.setState({
            email:'',
            pssw:'',
            loading:false,
            error:'registerSuccess',

          });
      console.log({email})
  }

  //Spinner und Button Interaktion wenn loading true dann spinner, ansonsten button onpress zur login
  renderButton(){

    if(this.state.loading){
      return <Spinner size="small"/>
    }
    return(
      <Button onPress={this.onButtonPress.bind(this)}>
        LogIn
      </Button>
    );

  }

  render(){

    return (

        <View style={styles.loggedInStyle}>
            <CoachCard >


              <CardSection>

                  <Input
                  label="Email: "
                  placeholder="user@gmail.com"
                  keyboardType="email-adress"
                  autoCapitalize="none"
                  autoFocus={true}
                  value={this.state.email}
                  onChangeText={(email) => this.setState({email})}/>

              </CardSection>


              <CardSection >

                  <Input
                  secureTextEntry
                  label="Passwort: "
                  returnKeyTyp="go"
                  placeholder="passwort"
                  value={this.state.pssw}
                  onChangeText={(pssw) => this.setState({pssw})}/>

              </CardSection>


              <Text style={styles.errorTextStyle}>{this.state.error}</Text>

              //Login Button and Spinner function in renderBUtton
              <CardSection>
                {this.renderButton()}
              </CardSection>

            </CoachCard>
        </View>

    );
  }
}


const styles={
  loggedInStyle:{
    flex:1,
    flexDirection: 'column',
    justifyContent: 'flex-end',

  },
errorTextStyle:{
  fontSize:20,
  alignSelf:'center',
  color:'red'

}

}

export default LoginForm;
