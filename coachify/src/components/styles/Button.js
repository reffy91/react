import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
//onPRess prop from parent element to give the button the obj func
// ermöglicht einen button dynmaisch zu halten und ständig leicht zu konfigurieen

//indem button element verschied funktinen übergeben werden können


//Linking als möglichkeit um urls egal wo zu öffnen

//children gets the text ccontained in the jsx button . wiederverwendbarkeit
const Button = ({onPress,children}) =>{
//destruction buttonstyle from Styles for attributin jsx element
const  {buttonStyle,textStyle}=styles;

  return(
    //Feedback für den user - touchevent auf den button
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Text style={textStyle}>{children}  </Text>
    </TouchableOpacity>
  );
};

const styles= {

    textStyle:{
    alignSelf:'center',
    fontWeight:'600',
    fontSize:16,
    color:'#007aff',
    paddingTop:10,
    paddingBottom:10,
 

  },

  buttonStyle:{
//expand element in fullsize
    flex:1,
    backgroundColor:'#fff',
    borderRadius:5,
    borderWidth:1,
    borderColor:'#007aff',
    marginLeft:5,
    marginRight:5
  }
};


export default Button;
