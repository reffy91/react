import React, {Component} from 'react';
import {View,TextInput,Text} from 'react-native';
import CoachCard from '../coachList/coachCard';
import CardSection from '../coachList/cardSection';
import Button from './Button';
import Header from '../header/header';

//Methode um die Attribute und Werte des elternelements, wo diese comp enthalten ist, werte in dieser
//compm laden kann
const Input = ({label,placeholder,secureTextEntry,value,onChangeText,autoFocus}) =>{

  const {inputStyle,labelStyle,containerStyle} = styles;

    //  {...this.props} to get all props from the jsx element in the parent
    //    secureTextEntry={true} same as     secureTextEntry attribute in jsx (only by boolean values)
  return(


    <View style={containerStyle} >
      <Text style={labelStyle}>{label}</Text>

      <TextInput
      onChangeText={onChangeText}
      value={value}
      secureTextEntry={secureTextEntry}
      placeholder={placeholder}
      autoCorrect={false}
      autoFocus={autoFocus}
      style={inputStyle}
      {...this.props}
    />
    </View>


  );
};
const styles={

  labelStyle:{
    fontSize:18,
    paddingLeft:20,

    flex:1// take just onlye one coloumnsspaces
  },
  inputStyle:{
    color:'#000',
    paddingRight:5,
    paddingLeft:5,
    fontSize:18,
    lineHeight:10, //increase space between text to stand alone
    flex:2          //text input are children of View we say : for each sibling add 2 for 2 coloumnsspaces
  },
  containerStyle:{
    height:40,
    flex:1,
    flexDirection:'row',
    alignItems:'center',
  }
};

export default Input;
