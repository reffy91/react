//import library
import React from 'react';
import { Text,StyleSheet,View} from 'react-native';



//Create Component
//props: Obj parameter der daten von eltern elementen erhaelt
const Header = (props)=>{

  return(

    <View style={[styles.header]}>
      <Text style={[styles.fonts]}>{props.headerText}</Text>
    </View>

  );
};


//Stylesheet
const styles = StyleSheet.create({
  header:{
    height:66,
    backgroundColor: '#eee',
    shadowColor: '#000',
    shadowOffset:{width:0,height:2},
    shadowOpacity:0.9,
    elevation:2,
    position:'relative',
    bottom:0
  },

  fonts:{
    color:'#01579b',
    fontWeight:'bold',
    fontSize:30,
    padding:10,


  }



});

//make comp avaibale to the other components in references
export default Header;


//stylesheet : Layout,Padding, Margin, font, text-align b-/color,
// gradient,
